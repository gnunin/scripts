#!/bin/bash

# Rofi Based Ricing script for i3 Window Manager
# Developed by Matthew Weber AKA The Linux Cast
# Version 0.3
# Last Updated 07 Aug 2022

#Variables & Paths

alconf="$HOME/dotfiles/alacritty/"
rofi="$HOME/dotfiles/rofi/"
walls="$HOME/images/wallpapers/rice/"
piconf="$HOME/dotfiles/picom/picom.conf"
dunst="$HOME/dotfiles/dunst/dunstrc.d/"
bspwmconf="$HOME/dotfiles/bspwm/"
vimconf="$HOME/dotfiles/nvim/"

declare -a options=(
"catppuccin"
"dracula"
"everforest"
"gruvbox"
"nord"
"ocean"
"quit"
)

choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

if [ $choice = 'quit' ]; then
    echo "No theme selected"
    exit
fi

#Copy Config Files to the Appropriate Places. Placeholder files must exist.
cp $rofi/$choice.rasi $rofi/theme.rasi
cp $dunst/01-$choice.conf $dunst/99-theme.conf
cp $alconf/colors/$choice.yml $alconf/colors.yml
cp $bspwmconf/themes/$choice.conf $bspwmconf/theme.conf
sh $HOME/scripts/nvim/$choice.sh

# Set Wallpapers
feh --bg-fill $walls/$choice.jpg

#Dunst (Disable if you do not use dunst)
killall dunst
while pgrep -u $UID -x dunst >/dev/null; do sleep 1; done

bspc wm -r
