#!/bin/bash

colors="$HOME/dotfiles/alacritty/colors.yml"
schemes="$HOME/dotfiles/alacritty/colors"

declare -a options=(
"catppuccin"
"dracula"
"everforest"
"gruvbox"
"nord"
"ocean"
"quit"
)

choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

cp $schemes/$choice.yml $colors
