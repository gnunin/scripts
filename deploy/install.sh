echo "Abe deployment"

echo "Installing Base Devel"
sudo pacman -S --needed base-devel

echo "Install yay"
git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si 

echo "Installing software"
cd $HOME && git clone https://gitlab.com/gnunin/dotfiles 
pacman -Syy $(cat $HOME/dotfiles/deploy/pkglist | perl -pe 's|\n| |g') -y

echo "Switch to fish shell"
chsh -s $(which fish)

echo "Github"


echo "Symlink"
chmod +x $HOME/dotfiles/deploy/symlink.sh
./$HOME/dotfiles/deploy/symlink.sh &&

echo "Services"
sudo systemctl enable pcscd.service
sudo systemctl start pcscd.service

echo "Locale"
sudo printf "sv_SE.UTF-8 UTF-8\n" >> /etc/locale.gen
sudo locale-gen

echo "Font installation"
fc-cache -f -v

echo "Set path"
export PATH="$HOME/.local/bin:$PATH"

echo "Installation finished, rebooting"
sudo reboot


